Bootstrap: docker
From: ubuntu:focal
Stage: build
%post
  apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends  -y \
  pkg-config \
  cmake \
  build-essential \
  libhdf5-dev \
  libgsl-dev \
  libgtk-3-dev \
  libcairo2-dev \
  libeigen3-dev \
  libpango1.0-dev \
  libgdk-pixbuf2.0-dev \
  libfftw3-dev \
  libncurses-dev \
  libpng-dev \
  libtiff5-dev \
  git \
  flex \
  bison \
  libzmq3-dev \
  libmsgpack-dev \
  python3-dev \
  python3-pip \
  unzip \
  wget \
  curl \
  ninja-build \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

  cd /root

  pip3 install meson

  mkdir /home/crystfel-build
  cd    /home/crystfel-build

  # Mosflm
  wget -nv https://www.mrc-lmb.cam.ac.uk/mosflm/mosflm/ver740/pre-built/mosflm-linux-64-noX11.zip
  unzip mosflm-linux-64-noX11.zip
  mv mosflm-linux-64-noX11 /usr/local/bin/mosflm

  # CrystFEL
  git clone https://gitlab.desy.de/thomas.white/crystfel.git
  cd crystfel && meson build -Dprefix=/usr/local
  ninja -C build
  ninja -C build test
  ninja -C build install

## Stage 2
Bootstrap: docker
From: ubuntu:focal
Stage: final

%files from build
  /usr/local/* /usr/local/

# Environment variable needed for CrystFEL GUI and Mosflm
# The file is installed by libccp4c, a wrapped subproject of CrystFEL
%environment
  SYMINFO=/usr/share/ccp4/syminfo.lib

%post
  apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
  libhdf5-103 \
  libgsl23 \
  libgtk-3-0 \
  libcairo2 \
  libpango1.0 \
  libgdk-pixbuf2.0 \
  libfftw3-double3 \
  libncurses6 \
  libpng16-16 \
  libtiff5 \
  libzmq5 \
  libmsgpackc2 \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

  ldconfig
